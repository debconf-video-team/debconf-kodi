TARGET_DIR = .

VERSION=$(shell grep "addon id" addon.xml |cut -d '"' -f 6)

zip:
	git archive --prefix=plugin.video.debconf/ HEAD --format=zip -0 -o $(TARGET_DIR)/plugin.video.debconf-$(VERSION).zip addon.xml LICENSE.txt resources

clean:
	$(RM) $(TARGET_DIR)/plugin.video.debconf-$(VERSION).zip
